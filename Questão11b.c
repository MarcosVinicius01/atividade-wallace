#include <stdio.h>

int main()
{
    char nome;
    int num1, num2;
    int div, soma, sub, mult;

    printf("digite qual a operacao: ");
    scanf("%c", &nome);

    printf("digite o primeiro numero: ");
    scanf("%d", &num1);

    printf("digite o segundo numero: ");
    scanf("%d", &num2);

    div = num1 / num2;
    soma = num1 + num2;
    sub = num1 - num2;
    mult = num1 * num2;
    
    switch(nome)
    {
    case '+':
        printf("o resultado da soma dos dois numeros e: %d", soma);   
        break;

    case '-':
        printf("o resultado da subtracao dos dois numeros e: %d", sub);
        break;

    case '*':
        printf("o resulta da multiplicacao dos dois numeros e: %d", mult);
        break;

    case '/':
        printf("o resultado da divisao dos dois numeros e: %d", div);
        break;

    default:
        printf("operacao invalida.");
        break;
    }
}
