#include <stdio.h>

int main()
{
    char numero[10];
    int i = 0;
    int num, maior, menor;

    for(i = 0; i < 10; i++){

        printf("digite o valor do vetor na posicao %d: \n", i);
        scanf("%d", &num);

        numero[i] = num;

    }

    maior = numero[0];

    for(i = 1; i < 10; i++){
        if(numero[i] > maior)
        {
            maior = numero[i];
        }
    }
    printf("o maior numero do vetor e: %d\n", maior);

    menor = numero[0];

    for(i = 1; i < 10; i++){
        if(numero[i] < menor)
        {
            menor = numero[i];
        }
    }
    printf("o menor numero do vetor e: %d", menor);
}
