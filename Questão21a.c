#include <stdio.h>
#include <string.h>

int main()
{
    char nome[50];
    int i = 0;
    int tamanho;

    printf("digite o texto: ");
    scanf("%s", &nome);

    tamanho = strlen(nome);

    printf("o nome em maiusculo fica ");
    for(i = 0; i <= tamanho; i++)
    {
        nome[i] = toupper(nome[i]);
        printf("%c", nome[i]);

    }

    printf("\n");
    printf("o nome em minusculo fica ");
    for(i = 0; i <= tamanho; i++)
    {
        nome[i] = tolower(nome[i]);
       printf("%c", nome[i]);
    }
    
}
