#include <stdio.h>

int main()
{
    //variaveis 
    float bruto, liquido, imposto, hora, valorhora, desconto;

    //recebe a quantidade de horas trabalhada por dia
    printf("informe quantas horas voce trabalha diariamente: ");
    scanf("%f", &hora);

    //recebendo o valor de horas trabalhadas
    printf("informe o valor da sua hora e trabalho: ");
    scanf("%f", &valorhora);

    //multiplicando a hora por 30 (1 mês)
    hora = hora * 30;
    //calculando o valor mensal bruto recebido
    bruto = hora * valorhora;

    //verificando se o salario corresponde aos diferente tipos de aliquotas do INSS
    if(bruto <= 1045)
    {
        liquido = (bruto * 92.5)/100.0;
        desconto = bruto - liquido;

        printf("o valor do salario bruto e: R$%.2f\n", bruto);
        printf("o valor do desconto de 7,5%% do inss e: R$%.2f\n", desconto);
        printf("o valor do salario liquido e: R$%.2f\n", liquido);
    }

    if(bruto > 1045 && bruto <= 2089.60)
    {
        liquido = (bruto * 91.0)/100.0;
        desconto = bruto - liquido;

        printf("o valor do salario bruto e: R$%.2f\n", bruto);
        printf("o valor do desconto de 7,5%% do inss e: R$%.2f\n", desconto);
        printf("o valor do salario liquido e: R$%.2f\n", liquido);       
    }

    if(bruto > 2089.61 && bruto <= 3134.40)
    {
        liquido = (bruto * 88.0)/100.0;
        desconto = bruto - liquido;

        printf("o valor do salario bruto e: R$%.2f\n", bruto);
        printf("o valor do desconto de 7,5%% do inss e: R$%.2f\n", desconto);
        printf("o valor do salario liquido e: R$%.2f\n", liquido);       
    }

    if(bruto > 3134.41 && bruto <= 6101.06)
    {
        liquido = (bruto * 86.0)/100.0;
        desconto = bruto - liquido;

        printf("o valor do salario bruto e: R$%.2f\n", bruto);
        printf("o valor do desconto de 7,5%% do inss e: R$%.2f\n", desconto);
        printf("o valor do salario liquido e: R$%.2f\n", liquido);       
    }

}
