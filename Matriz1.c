#include <stdio.h>

int main()
{
    //Declarando a matriz 3x3 de inteiros
    int matriz[3][4], linha, coluna;

    //loop para captar os 9 numeros em uma matriz
    //indice para as linhas
    for(linha = 0; linha < 3; linha++)
    {
        //indice para as colunas
        for(coluna = 0; coluna < 4; coluna++)
        {
            printf("Digite um numero: \n");
            scanf("%d", &matriz[linha][coluna]);
        }
    }    
    //mostrar os valores linha por linha e coluna separados por traço
      for(linha = 0; linha < 3; linha++)
    {
        for(coluna = 0; coluna < 4; coluna++)
        {
            printf("%d - ", matriz[linha][coluna]);
            
        }
        printf("\n");
    }  

}
