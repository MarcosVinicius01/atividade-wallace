/*⦁	Uma companhia quer verificar se um empregado está qualificado para a aposentadoria. Para estar em condições, um dos seguintes requisitos deve ser satisfeito: 
  * Ter no mínimo 65 anos de idade. 
  * Ter trabalhado, no mínimo 30 anos. 
  * Ter no mínimo 60 anos e ter trabalhado no mínimo 25 anos. 
	Ler os dados: o ano de nascimento do empregado e o ano de seu ingresso na companhia. O programa deverá escrever a idade e o tempo de trabalho do empregado e a mensagem “Requerer aposentadoria” ou “Não requerer”.
Obs.: Utilize como ano atual o ano de 2011*/

#include <stdio.h>

int main()
{
    int idade, trab, ingresso;

    printf("Digite sua idade: ");
    scanf("%d", &idade);

    printf("digite o ano de ingresso na companhia: ");
    scanf("%d", &trab);

    ingresso = 2011 - trab;

    printf("voce possui %d anos, e trabalhou %d anos para a companhia.\n", idade, ingresso);

    if(idade < 60 && ingresso < 25)
    {
        printf("Nao requer aposentaria.");
    }
    else 
    {
        printf("requer aposentadoria.");
    }
    
    return 0;
}
