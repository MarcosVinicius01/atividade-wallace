#Include <stdio.h>

int main(){
    char resp;
    int num1, num2, soma;

    do
    {
        printf("digite o primeiro numero: \n");
        scanf("%d", &num1);

        printf("digite o segundo numero: \n");
        scanf("%d", &num2);

        soma = (num1 + num2);

        printf("a soma dos numero e: %d\n", soma); 

        getchar(); // exclui o enter dado anteriormente //
        printf("deseja realizar um novo calculo? s/n: ");
        scanf("%c", &resp);

    } while (resp == 's' || resp == 'S');
    
}
