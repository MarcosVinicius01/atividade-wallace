#include <stdio.h>

int main()
{
    int base, expo;
    int i = 0;
    int mult = 1;

    printf("digite o valor da base: ");
    scanf("%d", &base);

    printf("digite o valor do expoente: ");
    scanf("%d", &expo);

    while(i != expo)
    {

        mult = mult * base;
        
        i++;

    }
    printf("o resultado de %d elevado a %d e: %d\n", base, expo, mult);
}
