#include <stdio.h>

int main() {
	float matriz[10][3];
	int lin, col;
	//matriz 10x3 de float
		
		//loop de 10x
    printf("---Digite suas notas---\n ");
	for(lin = 0; lin < 10; lin++){
		//lin1 = nota1
		printf("Digite a nota1: \n");
		scanf("%f", &matriz[lin][0]);
		//lin2 = nota2 
		printf("Digite a nota2: \n");
		scanf("%f", &matriz[lin][1]);
		//lin3 = media
		matriz[lin][2] = ((matriz [lin][0] + matriz [lin][1]) /2.0);
	}
    printf("\n---Suas notas foram---\n");
	printf("nota1   nota2   nota3  media \n");
	for(lin = 0; lin < 10; lin++){
		for(col = 0; col < 3; col++){
			printf("%2.f\t", matriz[lin][col]);
		}
		printf("\n");
	}
}
